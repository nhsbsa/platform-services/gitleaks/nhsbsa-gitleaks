import re

def test_diff_additions():
    
    urls = [
        'https://abcdefghij.execute-api.us-east-2.amazonaws.com/dev/hello-world',
        'https://abcdefghij.execute-api.us-east-2.amazonaws.com/dev/hello_world#anchor',
        'https://abcdefghij.execute-api.us-east-2.amazonaws.com/dev/hello-world?this=that',
        'https://abcdefghij.execute-api.us-east-2.amazonaws.com/dev/hello-world#anchor?this=that',
        'jdbc:postgresql://user:password@mypostgresql.abcdefghijklmn.us-east-2.rds.amazonaws.com:5432/mydatabase',
    ]
    
    scheme = r'(?:(?:[a-zA-Z0-9+\-.]*:)+(?:\/){1,2})'
    auth = r'(?:(?:(?:[^@\/?#\s\-_]+)@)?(?:[^\/?#\s]+)?)'
    path = r'(?:\/[^\?^#.]+)*'
    anchor = r'(?:[#][^\?.]*)?'
    query = r'(?:[\?](?:[^#\s]+))?'
        
    #pattern = r'(?:(?:([^@\/?#\s\-_]+)@)?([^\/?#\s]+)?(?:\/([^?#\-\s]*))?(?:[?]([^#\s]+))?'
    
    pattern = f'({scheme}{auth}{path}{anchor}{query})'
    print(pattern)
    
    for string in urls:
        match = re.match(pattern=pattern, string=string)
        print(match.groups())
    
    
    
if __name__ == "__main__":
    test_diff_additions()