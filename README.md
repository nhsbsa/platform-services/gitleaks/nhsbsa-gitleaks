# NHSBSA Gitleaks configuration

Standard Gitleaks configuration files for NHSBSA projects.

The project builds on the standard [Gitleaks.toml](https://raw.githubusercontent.com/zricethezav/gitleaks/master/config/gitleaks.toml) config file with additional rule definitions.

It ensures:

* rules are tested with real-world secrets (revoked)
* false positives are identified and tested for exclusion when possible
* only verified rules are exported

## Usage

For further detail on Gitleaks usage, consult [the documentation](https://github.com/zricethezav/gitleaks)

### Add Gitleaks configuration

To use the NHSBSA Gitleaks definitions:

* Copy the [gitleaks-nhsbsa.toml](https://gitlab.com/nhsbsa/platform-services/gitleaks/gitleaks-nhsbsa/-/raw/main/gitleaks-nhsbsa.toml) config file into your project

```bash
wget https://gitlab.com/nhsbsa/platform-services/gitleaks/gitleaks-nhsbsa/-/raw/main/gitleaks-nhsbsa.toml
```

* Create a project specific `gitleaks.toml` file and extend the NHSBSA file

```toml
title = "<project-name> gitleaks config"

[extend]
path = "gitleaks-nhsbsa.toml"
```

### Add a pre-commit hook

* Install [pre-commit](https://pre-commit.com/)
* Install [Gitleaks](https://github.com/zricethezav/gitleaks)
* Add a `.pre-commit-config.yaml` configuration file:

```yaml
repos:
-   repo: local
    hooks:
    - id: gitleaks
      name: Detect hardcoded secrets
      description: Detect hardcoded secrets using Gitleaks
      entry: gitleaks protect --config="gitleaks.toml" --verbose --redact --staged
      language: system
      pass_filenames: false
```

* Every contributor must install the Git hook using pre-commit:

```bash
pre-commit install
```

### Configure Gitlab secrets detection

* Include Gitleaks secret detection job template in `gitlab-ci.yml`

```yaml
include:
  - template: Security/Secret-Detection.gitlab-ci.yml
secret_detection:
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "true"
```

* Instruct secrets detection analyzer to use the project specific Gitleaks config toml file
  Create a file `.gitlab/secret-detection-ruleset.toml`

```yaml
[secrets]
description = 'secrets custom rules configuration'

[[secrets.passthrough]]
type  = "file"
target = "gitleaks.toml"
value = "gitleaks.toml"
```

## Contributing

Only tested and verified rules will be exported into the NHSBSA standard config

### To test a Gitleaks rule

* Create a `yaml` fixture file in the `secrets` folder
* Name the file after the `rule-id` you are testing. e.g. `my-rule-id.yml`
* Follow this format:

```yaml
secrets:
  secret-scenario-1: "<secret>"
  secret-scenario-2:
    filename: "pom.xml"
    secret: "api_key_<secret>"
allowed:
  allowed-scenario-1: "false_positive_value"
```

Secrets and allowed values can be defined as simple name-value pairs, or using the extended format, with a nested secret and filename.

* Consider:
  * Name your scenarios so they make sense. e.g. `xyz-api-token` or `xyz-api-token-prefixed`
  * Use [YAML multiline formatting](https://yaml-multiline.info/) if required

* Run our docker [gitleaks-py](https://gitlab.com/nhsbsa/platform-services/gitleaks/gitleaks-py) image to test the rule

```bash
docker run -it -v `pwd`:/gitleaks registry.gitlab.com/nhsbsa/platform-services/gitleaks/gitleaks-py:0.6.1 verify -s secrets toml/*.toml
```

* If the command passes without failure, run this command to update the standard file:

```bash
docker run -it -v `pwd`:/gitleaks registry.gitlab.com/nhsbsa/platform-services/gitleaks/gitleaks-py:0.6.1 merge -s secrets -d gitleaks-nhsbsa.toml toml/*.toml https://raw.githubusercontent.com/zricethezav/gitleaks/master/config/gitleaks.toml
```

* Commit the updated file your branch, push to Gitlab and submit a Merge Request

### To create a new Gitleaks rule

Always favour an existing Gitleaks rule, and simply provide a test. Sometimes the rules aren't correct, or just don't exist. In these cases create a new rule in this repository.

__Consider [submitting the new rule](https://github.com/zricethezav/gitleaks#configuration) for inclusion in the Gitleaks standard file__

* Create a new config file in the `toml` directory

```toml
title = "<secret supplier>"

[[rules]]
my-rule-id"
description = "my rule description"
regex = "<the capturing regex>"
keywords = [ "<keyword-1>" ]
secretGroup = <the regex capture group to use for entropy>
entropy = 3.5
```

* Keep the title as one word for the supplier. e.g. aws. The merge process will concatenate all titles into one.
* Add as many rules as are relevant to the supplier's products.
  For instance our [nhsbsa-aws.toml](docs/nhsbsa-aws.toml) file defines rules for `aws-access-key-id` and `aws-secret-access-key`
* Do not define a global `allowlist`. These are defined in our [nhsbsa-global.toml](docs/nhsbsa-global.toml) file
* Test the new rule as shown above
* Refer to [TOML reference docs](https://toml.io/en/) for formatting help

### Regex development

Regexes can be difficult to debug. Use the `regex_py` folder to develop regexes.

e.g.

```bash
python3 -m regex_py.url_regex
```

## Coverage

These rules have been verified with tests in the latest build:

| Source | Rule ID |Supplier docs |
|--------|---------|----------------------|
| NHSBSA | aws-access-key-id | https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-about |
| NHSBSA | aws-secret-access-key | https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-about |
| NHSBSA | datadog-api-key | https://docs.datadoghq.com/account_management/api-app-keys/ |
| NHSBSA | datadog-application-key | https://docs.datadoghq.com/account_management/api-app-keys/ |
| NHSBSA | docker-personal-access-token | https://docs.docker.com/docker-hub/access-tokens/ |
| Gitleaks | generic-api-key | - |
| Gitleaks | github-app-token | https://docs.github.com/en/developers/apps/building-github-apps/identifying-and-authorizing-users-for-github-apps |
| Gitleaks | github-fine-grained-pat | https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token |
| Gitleaks | github-pat | https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token |
| Gitleaks | github-refresh-token | https://docs.github.com/en/developers/apps/building-github-apps/refreshing-user-to-server-access-tokens |
| Gitleaks | gitlab-pat | https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html |
| NHSBSA | ip-v4-address | - |
| Gitleaks | jwt | https://jwt.io/ |
| Gitleaks | npm-access-token | https://docs.npmjs.com/about-access-tokens |
| Gitleaks | postman-api-token | https://learning.postman.com/docs/sending-requests/authorization/ |
| Gitleaks | private-key | - |
| Gitleaks | pypi-upload-token | https://pypi.org/help/#apitoken |
| NHSBSA | url | - |
| NHSBSA | uuid | - |
